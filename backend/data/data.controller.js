import {
  addTimeRecordingModel,
  editTimeRecordingModel,
  searchTimeRecordingsForProject,
  deleteTimeRecordingModel,
} from "./data.model.js";
import {
  recordDeletion,
  recordAddition,
  recordModification,
} from "../statistics/statistics.model.js";
import {
  createRecording,
  editRecording,
  deleteRecording,
  searchRecordings,
} from "../rabbit/publisher.js";

async function addTimeRecording(request, response) {
  let timeRecording = request.body;
  recordAddition(timeRecording.projectId, timeRecording.date);
  addTimeRecordingModel(timeRecording);
  await createRecording(timeRecording);
  response.json(timeRecording);
}
async function editTimeRecording(request, response) {
  let timeRecording = request.body;
  recordModification(timeRecording.projectId, timeRecording.date);
  const timeRecordingId = await editTimeRecordingModel(timeRecording);
  await editRecording(timeRecording);
  response.json(timeRecordingId);
}

async function deleteTimeRecording(request, response) {
  let id = request.params.timeRecordingId;
  recordDeletion(request.params.projectId, request.params.date);
  const timeRecordingId = await deleteTimeRecordingModel(id);
  await deleteRecording(
    request.params.projectId,
    request.params.employeeId,
    request.params.recordingTypeId,
    request.params.duration,
  );
  response.json(timeRecordingId);
}

async function searchTimeRecordings(request, response) {
  let projectId = request.params.projectId;
  const timeRecordings = await searchTimeRecordingsForProject(projectId);
  await searchRecordings(projectId);
  response.json(timeRecordings);
}

export {
  addTimeRecording,
  editTimeRecording,
  searchTimeRecordings,
  deleteTimeRecording,
};
