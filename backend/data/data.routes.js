import { Router } from "express";
import {
  addTimeRecording,
  editTimeRecording,
  searchTimeRecordings,
  deleteTimeRecording,
} from "./data.controller.js";

const router = Router();

router.post("/add", addTimeRecording);
router.put("/edit", editTimeRecording);
router.get("/search/:projectId", searchTimeRecordings);
router.delete(
  "/delete/:timeRecordingId/:projectId/:date/:employeeId/:recordingTypeId/:duration",
  deleteTimeRecording
);

export { router };
