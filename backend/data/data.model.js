import * as db from "../services/database.js";

async function searchTimeRecordingsForProject(projectId) {
  
  let recordings = await db.searchRecording(projectId)

  for (let recording of recordings) {
    let project = await db.getProjectFromId(recording.projectId);
    let employee = await db.getEmployeeFromId(recording.employeeId);
    let recordingType = await db.getRecordingTypeFromId(recording.recordingTypeId);
    recording.project = project[0].name;
    recording.employee = employee[0].firstname + " " + employee[0].lastname;
    recording.recordingType = recordingType[0].name;
    recording.duration = recording.duration + " h";
    recording.date = recording.date.toISOString().split("T")[0];
  }
  return recordings;
}
async function deleteTimeRecordingModel(timeRecordingId) {
  await db.removeRecording(timeRecordingId);
}

async function addTimeRecordingModel(timeRecording) {
  await db.addRecording(timeRecording);
}

async function editTimeRecordingModel(timeRecording) {
  await db.updateRecording(timeRecording);
  return timeRecording.timeRecordingId;
}

export {
  addTimeRecordingModel,
  editTimeRecordingModel,
  searchTimeRecordingsForProject,
  deleteTimeRecordingModel,
};
