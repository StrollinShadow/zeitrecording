import {
    calculateDeletedRecordsCount,
    calculateNewRecordsCount,
    calculateChangedRecordsCount,
  } from './statistics.model.js';
  
  async function getDeletedRecords(req, res) {
    const { projectId, startDate, endDate } = req.params;
    try {
      const count = await calculateDeletedRecordsCount(projectId, startDate, endDate);
      res.status(200).json({ count });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  
  async function getNewRecords(req, res) {
    const { projectId, startDate, endDate } = req.params;
    try {
      const count = await calculateNewRecordsCount(projectId, startDate, endDate);
      res.status(200).json({ count });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  
  async function getChangedRecords(req, res) {
    const { projectId, startDate, endDate } = req.params;
    try {
      const count = await calculateChangedRecordsCount(projectId, startDate, endDate);
      res.status(200).json({ count });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  
  export {
    getDeletedRecords,
    getNewRecords,
    getChangedRecords,
  };
  