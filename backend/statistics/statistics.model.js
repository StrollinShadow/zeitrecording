import {
    incrementRecordsCount,
    getRecordsCount,
  } from '../services/database.js';
  
  // Funktion zum Inkrementieren der Zähler für gelöschte Zeiterfassungen
  async function recordDeletion(projectId, date) {
    const statisticTypeId = 1; // 1 für gelöscht
    await incrementRecordsCount(projectId, statisticTypeId, date);
  }
  
  // Funktion zum Inkrementieren der Zähler für neue Zeiterfassungen
  async function recordAddition(projectId, date) {
    const statisticTypeId = 2; // 2 für neu
    await incrementRecordsCount(projectId, statisticTypeId, date);
  }
  
  // Funktion zum Inkrementieren der Zähler für geänderte Zeiterfassungen
  async function recordModification(projectId, date) {
    const statisticTypeId = 3; // 3 für geändert
    await incrementRecordsCount(projectId, statisticTypeId, date);
  }
  
  // Funktion zum Abrufen der Anzahl gelöschter Zeiterfassungen
  async function calculateDeletedRecordsCount(projectId, startDate, endDate) {
    const statisticTypeId = 1; // 1 für gelöscht
    return await getRecordsCount(projectId, statisticTypeId, startDate, endDate);
  }
  
  // Funktion zum Abrufen der Anzahl neuer Zeiterfassungen
  async function calculateNewRecordsCount(projectId, startDate, endDate) {
    const statisticTypeId = 2; // 2 für neu
    return await getRecordsCount(projectId, statisticTypeId, startDate, endDate);
  }
  
  // Funktion zum Abrufen der Anzahl geänderter Zeiterfassungen
  async function calculateChangedRecordsCount(projectId, startDate, endDate) {
    const statisticTypeId = 3; // 3 für geändert
    return await getRecordsCount(projectId, statisticTypeId, startDate, endDate);
  }
  
  export {
    recordDeletion,
    recordAddition,
    recordModification,
    calculateDeletedRecordsCount,
    calculateNewRecordsCount,
    calculateChangedRecordsCount,
  };
  