import { Router } from 'express';
import {
  getDeletedRecords,
  getNewRecords,
  getChangedRecords,
} from './statistics.controller.js';

const router = Router();

router.get('/deletedRecords/:projectId/:startDate/:endDate', getDeletedRecords);
router.get('/newRecords/:projectId/:startDate/:endDate', getNewRecords);
router.get('/changedRecords/:projectId/:startDate/:endDate', getChangedRecords);

export { router };
