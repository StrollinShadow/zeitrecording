import {
  calculateTotalTimeProject,
  calculateTotalTimeEmployee,
  calculateTotalTimeRecordingType,
} from "./calculation.model.js";

async function calculateProject(req, res) {
  const { projectId, startDate, endDate } = req.params;
  try {
    const totalTime = await calculateTotalTimeProject(
      projectId,
      startDate,
      endDate
    );
    res.status(200).json({ totalTime });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}

async function calculateEmployee(req, res) {
  const { projectId, employeeId, startDate, endDate } = req.params;
  try {
    const totalTime = await calculateTotalTimeEmployee(
      projectId,
      employeeId,
      startDate,
      endDate
    );
    res.status(200).json({ totalTime });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}

async function calculateRecordingType(req, res) {
  const { projectId, recordingTypeId, startDate, endDate } = req.params;
  try {
    const totalTime = await calculateTotalTimeRecordingType(
      projectId,
      recordingTypeId,
      startDate,
      endDate
    );
    res.status(200).json({ totalTime });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}

export {
  calculateProject,
  calculateEmployee,
  calculateRecordingType,
};
