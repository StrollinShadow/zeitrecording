import * as db from "../services/database.js";

// Funktion zum Entfernen der Zeitkomponente eines Datums
function stripTime(date) {
  const d = new Date(date);
  d.setUTCHours(0, 0, 0, 0); // UTC-Stunden setzen, um Zeitzonenunterschiede zu vermeiden
  return d;
}

async function calculateTotalTimeProject(projectId, startDate, endDate) {
  let recordings = await db.searchRecording(projectId);
  const start = stripTime(startDate);
  const end = stripTime(endDate);
  let totalTime = 0;

  recordings.forEach((recording) => {
    const recordingDate = stripTime(recording.date);

    if (
      recording.projectId == projectId &&
      recordingDate >= start &&
      recordingDate <= end
    ) {
      totalTime += parseInt(recording.duration);
    }
  });

  return totalTime;
}

async function calculateTotalTimeEmployee(
  projectId,
  employeeId,
  startDate,
  endDate
) {
  let recordings = await db.searchRecording(projectId);
  const start = stripTime(startDate);
  const end = stripTime(endDate);
  let totalTime = 0;

  recordings.forEach((recording) => {
    const recordingDate = stripTime(recording.date);
    if (
      recording.projectId == projectId &&
      recording.employeeId == employeeId &&
      recordingDate >= start &&
      recordingDate <= end
    ) {
      totalTime += parseInt(recording.duration);
    }
  });

  return totalTime;
}

async function calculateTotalTimeRecordingType(
  projectId,
  recordingTypeId,
  startDate,
  endDate
) {
  let recordings = await db.searchRecording(projectId);
  const start = stripTime(startDate);
  const end = stripTime(endDate);
  let totalTime = 0;

  recordings.forEach((recording) => {
    const recordingDate = stripTime(recording.date);
    if (
      recording.projectId == projectId &&
      recording.recordingTypeId == recordingTypeId &&
      recordingDate >= start &&
      recordingDate <= end
    ) {
      totalTime += parseInt(recording.duration);
    }
  });

  return totalTime;
}

export {
  calculateTotalTimeProject,
  calculateTotalTimeEmployee,
  calculateTotalTimeRecordingType,
};
