import { Router } from "express";
import {
  calculateProject,
  calculateEmployee,
  calculateRecordingType,
} from "./calculation.controller.js";

const router = Router();

// Route für die Berechnung der Gesamtzeit für ein Projekt
router.get(
  "/totalTimeProject/:projectId/:startDate/:endDate",
  calculateProject
);

// Route für die Berechnung der Gesamtzeit für einen Mitarbeiter innerhalb eines Projekts
router.get(
  "/totalTimeEmployee/:projectId/:employeeId/:startDate/:endDate",
  calculateEmployee
);

// Route für die Berechnung der Gesamtzeit für eine Erfassungsart innerhalb eines Projekts
router.get(
  "/totalTimeRecordingType/:projectId/:recordingTypeId/:startDate/:endDate",
  calculateRecordingType
);

export { router };
