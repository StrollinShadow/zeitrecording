import amqp from "amqplib";
import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const EXCHANGE_NAME = "log_max_foll";

const startFileSubscriber = async () => {
  try {
    const connection = await amqp.connect("amqp://localhost");
    const channel = await connection.createChannel();

    await channel.assertExchange(EXCHANGE_NAME, "fanout", {
      durable: false,
      autoDelete: true,
    });

    const { queue } = await channel.assertQueue("", {
      exclusive: true,
    });

    await channel.bindQueue(queue, EXCHANGE_NAME, "");

    channel.consume(
      queue,
      (msg) => {
        if (msg.content) {
          const message = JSON.parse(msg.content.toString());
          const logDir = path.join(__dirname, "logs");
          if (!fs.existsSync(logDir)) {
            fs.mkdirSync(logDir);
          }
          const logFileName = path.join(
            logDir,
            `max_foll_${new Date().toISOString().split("T")[0]}.txt`
          );
          const logMessage = `${new Date().toISOString()} - ${
            message.eventType
          } - ${JSON.stringify(message)}\n`;
          fs.appendFileSync(logFileName, logMessage, "utf8");
        }
      },
      {
        noAck: true,
      }
    );

    console.log(" [*] Waiting for messages in %s.", queue);
  } catch (error) {
    console.error("Error in file subscriber:", error);
  }
};

// Start the subscriber
startFileSubscriber();
