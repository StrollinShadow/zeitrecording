import amqp from "amqplib";
import {
  getEmployeeFromId,
  getProjectFromId,
  getRecordingTypeFromId,
} from "../services/database.js";

const EXCHANGE_NAME = "log_max_foll";

const publishEvent = async (eventType, eventData) => {
  try {
    const connection = await amqp.connect("amqp://localhost");
    const channel = await connection.createChannel();

    await channel.assertExchange(EXCHANGE_NAME, "fanout", {
      durable: false,
      autoDelete: true,
    });

    const message = JSON.stringify({
      eventType,
      ...eventData,
      timestamp: new Date().toISOString(),
    });

    channel.publish(EXCHANGE_NAME, "", Buffer.from(message));

    await channel.close();
    await connection.close();
  } catch (error) {
    console.error("Error publishing event:", error);
  }
};

const createRecording = async (timeRecording) => {
  const employee = await getEmployeeFromId(timeRecording.employeeId);
  const project = await getProjectFromId(timeRecording.projectId);
  const recordingType = await getRecordingTypeFromId(
    timeRecording.recordingTypeId
  );
  const employeeName = employee[0].name;
  const projectName = project[0].name;
  const recordingTypeName = recordingType[0].name;

  await publishEvent("Neuerfassung", {
    employeeName,
    projectName,
    recordingTypeName,
    duration: timeRecording.duration,
    comment: timeRecording.comment,
  });
};

const editRecording = async (timeRecording) => {
  const employee = await getEmployeeFromId(timeRecording.employeeId);
  const project = await getProjectFromId(timeRecording.projectId);
  const recordingType = await getRecordingTypeFromId(
    timeRecording.recordingTypeId
  );
  const employeeName = employee[0].name;
  const projectName = project[0].name;
  const recordingTypeName = recordingType[0].name;
  await publishEvent("Editieren", {
    employeeName,
    projectName,
    recordingTypeName,
    duration: timeRecording.duration,
    comment: timeRecording.comment,
  });
};

const deleteRecording = async (
  projectId,
  employeeId,
  recordingTypeId,
  duration
) => {
  const employee = await getEmployeeFromId(employeeId);
  const project = await getProjectFromId(projectId);
  const recordingType = await getRecordingTypeFromId(recordingTypeId);
  const employeeName = employee[0].name;
  const projectName = project[0].name;
  const recordingTypeName = recordingType[0].name;

  await publishEvent("Löschen", {
    employeeName,
    projectName,
    recordingTypeName,
    duration,
  });
};

const searchRecordings = async (projectId) => {
  const project = await getProjectFromId(projectId);
  console.log(project);
  if (project.lenght === 0) {
    return;
  }
  const projectName = project.name;

  await publishEvent("Suchen", {
    projectName,
  });
};

export { createRecording, editRecording, deleteRecording, searchRecordings };
