import amqp from "amqplib";

const EXCHANGE_NAME = "log_max_foll";
const MAX_EVENTS = 20;

const startConsoleSubscriber = async () => {
  try {
    const connection = await amqp.connect("amqp://localhost");
    const channel = await connection.createChannel();

    await channel.assertExchange(EXCHANGE_NAME, "fanout", {
      durable: false,
      autoDelete: true,
    });

    const { queue } = await channel.assertQueue("", {
      exclusive: true,
    });

    await channel.bindQueue(queue, EXCHANGE_NAME, "");

    let events = [];

    channel.consume(
      queue,
      (msg) => {
        if (msg.content) {
          const message = JSON.parse(msg.content.toString());
          events.push(message);
          if (events.length > MAX_EVENTS) {
            events.shift();
          }
          console.clear();
          console.log(" [x] Received events:");
          events.forEach((event, index) => {
            console.log(
              `${index + 1}. ${event.eventType} - ${JSON.stringify(event)}`
            );
          });
        }
      },
      {
        noAck: true,
      }
    );

    console.log(" [*] Waiting for messages in %s.", queue);
  } catch (error) {
    console.error("Error in console subscriber:", error);
  }
};

startConsoleSubscriber();
