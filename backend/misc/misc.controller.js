import { readEmployees, readProjects, readRecordingTypes, readStatisticType } from './misc.model.js';

async function readAllEmployees(request, response) {
    const employees = await readEmployees();
    response.json(employees);
}

async function readAllProjects(request, response) {
    const projects = await readProjects();
    response.json(projects);
}

async function readAllRecordingTypes(request, response) {
    const types = await readRecordingTypes();
    response.json(types);
}

async function readAllStatisticTypes(request, response) {
    const types = await readStatisticType();
    response.json(types);
}

export {
    readAllEmployees,
    readAllProjects,
    readAllRecordingTypes,
    readAllStatisticTypes,
};
