// Importieren der Datenbankfunktionen
import * as db from '../services/database.js';

// Asynchrone Funktionen zum Lesen der Daten aus der Datenbank
async function readEmployees() {
  return await db.readEmployees();
}

async function readProjects() {
  return await db.readProjects();
}

async function readRecordingTypes() {
  return await db.readRecordingTypes();
}
async function readStatisticType() {
  return await db.readStatisticType();
}

// Exportieren der asynchronen Funktionen
export {
  readEmployees,
  readProjects,
  readRecordingTypes,
  readStatisticType,
};
