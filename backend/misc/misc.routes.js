import { Router } from 'express';
import { readAllEmployees, readAllProjects, readAllRecordingTypes, readAllStatisticTypes } from './misc.controller.js';

const router = Router();

router.get('/employee', readAllEmployees);
router.get('/project', readAllProjects);
router.get('/type', readAllRecordingTypes);
router.get('/statistic', readAllStatisticTypes);


export { router };