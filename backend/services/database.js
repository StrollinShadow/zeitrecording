import mysql from "mysql2/promise";

// Datenbankkonfiguration
const dbConfig = {
  host: "sql11.freemysqlhosting.net",
  user: "sql11690338",
  password: "2yn4rDZNTG",
  database: "sql11690338",
};

// Verbindung zur Datenbank herstellen
const pool = mysql.createPool(dbConfig);

const readEmployees = async () => {
  const [rows] = await pool.query("SELECT * FROM max_employee");
  return rows;
};

const readProjects = async () => {
  const [rows] = await pool.query("SELECT * FROM max_project");
  return rows;
};

const readRecordingTypes = async () => {
  const [rows] = await pool.query("SELECT * FROM max_recording_type");
  return rows;
};

const readStatisticType = async () => {
  const [rows] = await pool.query("SELECT * FROM max_statistics_type");
  return rows;
};

const getStatisticTypeFromId = async (statisticTypeId) => {
  const [rows] = await pool.query(
    "SELECT * FROM max_statistics_type WHERE id = ?",
    [statisticTypeId]
  );
  return rows;
};

const getEmployeeFromId = async (employeeId) => {
  const [rows] = await pool.query("SELECT * FROM max_employee WHERE id = ?", [
    employeeId,
  ]);
  return rows;
};
const getProjectFromId = async (projectId) => {
  const [rows] = await pool.query("SELECT * FROM max_project WHERE id = ?", [
    projectId,
  ]);
  return rows;
};
const getRecordingTypeFromId = async (recordingTypeId) => {
  const [rows] = await pool.query(
    "SELECT * FROM max_recording_type WHERE id = ?",
    [recordingTypeId]
  );
  return rows;
};
const addRecording = async (recording) => {
  const [current] = await pool.query("SELECT * FROM max_timerecording");
  let newRecordingID = current.length + 1;
  const [rows] = await pool.query(
    "INSERT INTO max_timerecording (timeRecordingId, projectId, employeeId, recordingTypeId, date, duration, comment) VALUES (?,?, ?, ?, ?, ?, ?)",
    [
      null,
      recording.projectId,
      recording.employeeId,
      recording.recordingTypeId,
      recording.date,
      recording.duration,
      recording.comment,
    ]
  );
  return rows;
};

const removeRecording = async (recordingId) => {
  const [rows] = await pool.query(
    "DELETE FROM max_timerecording WHERE timeRecordingId = ?",
    [recordingId]
  );
  return rows;
};

const updateRecording = async (recording) => {
  const [rows] = await pool.query(
    "UPDATE max_timerecording SET projectId = ?, employeeId = ?, recordingTypeId = ?, date = ?, duration = ?, comment = ? WHERE timeRecordingId = ?",
    [
      recording.projectId,
      recording.employeeId,
      recording.recordingTypeId,
      recording.date,
      recording.duration,
      recording.comment,
      recording.timeRecordingId,
    ]
  );
  return rows;
};
const searchRecording = async (projectId) => {
  const [rows] = await pool.query(
    "SELECT * FROM max_timerecording WHERE projectId = ?",
    [projectId]
  );
  return rows;
};

// Inkrementiere die Zähler für gelöschte, neue und geänderte Zeiterfassungen
const incrementRecordsCount = async (projectId, statisticTypeId, date) => {
  const [rows] = await pool.query(
    "INSERT INTO max_statistics (statisticId, projectId, statisticTypeId, statisticDate) VALUES (?, ?, ?, ?)",
    [null, projectId, statisticTypeId, date]
  );
  return rows;
};

// Lese die Zähler für gelöschte, neue und geänderte Zeiterfassungen
const getRecordsCount = async (
  projectId,
  statisticTypeId,
  startDate,
  endDate
) => {
  const [rows] = await pool.query(
    "SELECT COUNT(*) as count FROM max_statistics WHERE projectId = ? AND statisticTypeId = ? AND statisticDate BETWEEN ? AND ?",
    [projectId, statisticTypeId, startDate, endDate]
  );
  return rows[0].count;
};

export {
  readEmployees,
  readProjects,
  readRecordingTypes,
  readStatisticType,
  addRecording,
  removeRecording,
  updateRecording,
  searchRecording,
  getEmployeeFromId,
  getProjectFromId,
  getRecordingTypeFromId,
  getStatisticTypeFromId,
  incrementRecordsCount,
  getRecordsCount,
};
