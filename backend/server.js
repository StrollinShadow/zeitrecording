import express from "express";
import session from "express-session";
import Keycloak from "keycloak-connect";
import { router as dataRouter } from "./data/data.routes.js";
import { router as miscRouter } from "./misc/misc.routes.js";
import { router as calculationRouter } from "./calculation/calculation.routes.js";
import { router as statisticsRouter } from "./statistics/statistics.routes.js";
import cors from "cors";

const app = express();
const port = 3000;
const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({ store: memoryStore });

// frontend is a separate component with a different
app.use(cors());

app.use(keycloak.middleware());

app.use(
  session({
    //secret: found in client timeRecordingBackend, Credentials, Client Secret
    secret: "icEizOAIOWPrC37B5M2khOp6ONlbJqHv",
    resave: false,
    saveUninitialized: true,
    store: memoryStore,
  })
);

// for parsing application/json in order to access data from post requests
app.use(express.json());

app.use("/api/data", dataRouter);
app.use("/api/misc", miscRouter);
app.use("/api/calc", calculationRouter);
app.use("/api/statistic", statisticsRouter);

app.get("/", (req, res) => {
  res.send("Welcome to my server!");
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
