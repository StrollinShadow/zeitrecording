import axios from 'axios';
import authenticationService from './authenticationService';

const API_URL = 'http://localhost:3000/api/statistic/';

const getDeletedRecordsCount = async (projectId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const url = `${API_URL}deletedRecords/${projectId}/${startDate}/${endDate}`;
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.count;
  } catch (error) {
    console.error('Error getting deleted records count:', error);
    throw error;
  }
};

const getNewRecordsCount = async (projectId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const url = `${API_URL}newRecords/${projectId}/${startDate}/${endDate}`;
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.count;
  } catch (error) {
    console.error('Error getting new records count:', error);
    throw error;
  }
};

const getChangedRecordsCount = async (projectId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const url = `${API_URL}changedRecords/${projectId}/${startDate}/${endDate}`;
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.count;
  } catch (error) {
    console.error('Error getting changed records count:', error);
    throw error;
  }
};

export default {
  getDeletedRecordsCount,
  getNewRecordsCount,
  getChangedRecordsCount,
};
