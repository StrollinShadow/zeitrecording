import axios from 'axios';
import authenticationService from './authenticationService';

const API_URL = 'http://localhost:3000/api/';
const baseUrlCalc = API_URL + 'calc/';

const calculateTotalTimeProject = async (projectId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const calcUrl = `${baseUrlCalc}totalTimeProject/${projectId}/${startDate}/${endDate}`;
    const response = await axios.get(calcUrl, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.totalTime;
  } catch (error) {
    console.error('Error calculating total time for project:', error);
    throw error;
  }
};

const calculateTotalTimeEmployee = async (projectId, employeeId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const calcUrl = `${baseUrlCalc}totalTimeEmployee/${projectId}/${employeeId}/${startDate}/${endDate}`;
    const response = await axios.get(calcUrl, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.totalTime;
  } catch (error) {
    console.error('Error calculating total time for employee:', error);
    throw error;
  }
};

const calculateTotalTimeRecordingType = async (projectId, recordingTypeId, startDate, endDate) => {
  try {
    let token = authenticationService.getToken();
    const calcUrl = `${baseUrlCalc}totalTimeRecordingType/${projectId}/${recordingTypeId}/${startDate}/${endDate}`;
    const response = await axios.get(calcUrl, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data.totalTime;
  } catch (error) {
    console.error('Error calculating total time for recording type:', error);
    throw error;
  }
};

export default {
  calculateTotalTimeProject,
  calculateTotalTimeEmployee,
  calculateTotalTimeRecordingType,
};
