import Keycloak from "keycloak-js";

const keycloak = new Keycloak({
  url: "http://localhost:8080",
  realm: "ims3i",
  clientId: "timeRecordingFrontend",
});

let isKeycloakInitialized = false;
const minimumValidityTimeInSeconds = 300;

const getToken = async () => {
  try {
    const valid = await keycloak.updateToken(minimumValidityTimeInSeconds);
    if (valid) {
      console.log("Token is valid.");
      return keycloak.token; // Token is valid, return it
    } else {
      console.error("Token is not valid, and could not be refreshed.");
      // Handle scenario where token is not valid and cannot be refreshed, if necessary
      return null; // Or handle this scenario differently
    }
  } catch (error) {
    console.error("Error refreshing token:", error);
    throw error; // Re-throw the error if you want to handle it further up the chain
  }
};

const initKeycloak = () => {
  let promise = new Promise(function (resolve, reject) {
    try {
      if (isKeycloakInitialized) {
        console.log("KeyCloak is already initialized");
      }
      console.log("initKeycloak");
      isKeycloakInitialized = true;
      keycloak
        .init({ onLoad: "login-required" })
        .then((authenticated) => {
          console.log(
            `User is ${authenticated ? "authenticated" : "not authenticated"}`
          );
          console.log("token received:", keycloak.token);
          resolve({
            token: keycloak.token,
            roles: keycloak.tokenParsed.realm_access.roles,
          });
        })
        .catch((error) => {
          console.error("Error 1 initializing Keycloak:", error);
          reject(-1);
        });
    } catch (error) {
      console.error("Error 2 initializing Keycloak:", error);
      reject(-1);
    }
  });
  return promise;
};

export default {
  initKeycloak,
  getToken,
};
