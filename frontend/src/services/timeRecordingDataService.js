import axios from "axios";
import authenticationService from "./authenticationService";

const API_URL = "http://localhost:3000/api/";
const baseUrlData = API_URL + "data/";

const addTimeRecording = async (timeRecording) => {
  let token = authenticationService.getToken();
  const url = baseUrlData + "add";
  const response = axios.post(url, timeRecording, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;
    return data;
  });
};

const editTimeRecording = async (timeRecording) => {
  let token = authenticationService.getToken();
  const url = baseUrlData + "edit";
  const response = axios.put(url, timeRecording, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;
    return data;
  });
};

const deleteTimeRecording = async (timeRecording) => {
  let token = authenticationService.getToken();
  const url =
    baseUrlData +
    "delete/" +
    timeRecording.timeRecordingId +
    "/" +
    timeRecording.projectId +
    "/" +
    timeRecording.date +
    "/" +
    timeRecording.employeeId +
    "/" +
    timeRecording.recordingTypeId +
    "/" +
    timeRecording.duration +
    "/";
  const response = axios.delete(url, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;
    return data;
  });
};

const searchTimeRecordings = async (projectId) => {
  try {
    let token = authenticationService.getToken();
    const url = `${baseUrlData}search/${projectId}`;
    const response = await axios.get(url, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.error("Failed to search time recordings:", error);
    throw error; // Rethrow or handle as needed
  }
};

export default {
  addTimeRecording,
  editTimeRecording,
  searchTimeRecordings,
  deleteTimeRecording,
};
