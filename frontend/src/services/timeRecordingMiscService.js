import axios from "axios";
import authenticationService from "./authenticationService";
const baseUrlData = "http://localhost:3000/api/misc/";

const readAllEmployees = async () => {
  let token = authenticationService.getToken();
  const response = axios.get(baseUrlData + "employee/", {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;

    return data;
  });
};

const readAllProjects = () => {
  let token = authenticationService.getToken();

  const response = axios.get(baseUrlData + "project/", {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;

    return data;
  });
};

const readAllRecordingTypes = async () => {
  let token = authenticationService.getToken();
  const url = baseUrlData + "type/";
  const response = axios.get(url, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;

    return data;
  });
};
const readAllStatisticTypes = async () => {
  let token = authenticationService.getToken();
  const url = baseUrlData + "statistic/";
  const response = axios.get(url, {
    headers: {
      authorization: `Bearer ${token}`,
    },
  });
  return response.then((res) => {
    let data = res.data;

    return data;
  });
};

export default {
  readAllEmployees,
  readAllProjects,
  readAllRecordingTypes,
  readAllStatisticTypes,
};
