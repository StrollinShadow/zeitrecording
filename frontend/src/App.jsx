import React, { useState, useEffect } from "react";
import "../src/styles/App.css";
import TimeRecordingData from "./components/TimeRecordingData";
import TimeRecordingSearch from "./components/TimeRecordingSearch";

import Calculation from "./components/Calculation";
import Statistics from "./components/Statistics";
import Welcome from "./components/Welcome";

import timeRecordingDataService from "./services/timeRecordingDataService";
import Menu from "./components/Menu.jsx";

import miscService from "./services/timeRecordingMiscService";

import authenticationService from "./services/authenticationService.js";

function App() {
  const [roles, setRoles] = useState([]);
  const [token, setToken] = useState([]);
  const [authenticated, setAuthenticated] = useState(false);
  const [employees, setEmployees] = useState([]);
  const [projects, setProjects] = useState([]);
  const [recordingTypes, setRecordingTypes] = useState([]);
  const [timeRecordings, setTimeRecordings] = useState([]);
  const [activeComponent, setActiveComponent] = useState("welcome");

  const [selectedTimeRecording, setSelectedTimeRecording] = useState(null);
  const [timeRecording, setTimeRecording] = useState({
    projectId: "2",
    employeeId: "1",
    recordingTypeId: "2",
    date: new Date().toISOString().split("T")[0],
    duration: 4,
    comment: "Comment",
  });

  useEffect(() => {
    authenticationService
      .initKeycloak()
      .then((authData) => {
        setToken(authData.token);
        setAuthenticated(!!authData.token && authData.token.length > 0);
        setRoles(authData.roles || []);
      })
      .catch((error) => {
        console.error("Error initializing Keycloak:", error);
        setAuthenticated(false);
      });
  }, []);

  useEffect(() => {
    if (token.length > 0) {
      readEmployees();
      readRecordingTypes();
      readProjects();
    }
  }, [token]);
  const readEmployees = () => {
    miscService
      .readAllEmployees()
      .then((data) => {
        setEmployees(data);
      })
      .catch((error) => console.error("Error reading employees:", error));
  };

  const readRecordingTypes = () => {
    miscService
      .readAllRecordingTypes()
      .then((data) => {
        setRecordingTypes(data);
      })
      .catch((error) => console.error("Error reading recording types:", error));
  };

  const readProjects = () => {
    miscService
      .readAllProjects()
      .then((data) => {
        setProjects(data);
      })
      .catch((error) => console.error("Error reading projects:", error));
  };

  const refreshTimeRecordings = async () => {
    try {
      const updatedRecordings =
        await timeRecordingDataService.searchTimeRecordings(); // or whatever method you use to fetch recordings
      // Assuming you have a setState method to update your recordings list
      setTimeRecordings(updatedRecordings);
    } catch (error) {
      console.error("Failed to refresh time recordings:", error);
    }
  };
  if (!token) {
    return <div>Initializing Keycloak...</div>;
  }
  const handleEditTimeRecording = (recording) => {
    setSelectedTimeRecording(recording);
    setActiveComponent("timeRecordingInput");
  };
  const editCancel = () => {
    setSelectedTimeRecording(null);
    setActiveComponent("timeRecordingSearch");
  };

  return (
    <div>
      {authenticated ? (
        <>
          <Menu setActiveComponent={setActiveComponent} roles={roles} />
          <div>
            {activeComponent === "welcome" && <Welcome />}
            {activeComponent === "timeRecordingInput" &&
              roles.includes("admin") && (
                <>
                  <TimeRecordingData
                    employees={employees}
                    projects={projects}
                    recordingTypes={recordingTypes}
                    timeRecording={timeRecording}
                    setTimeRecording={setTimeRecording}
                    refreshTimeRecordings={refreshTimeRecordings}
                    selectedTimeRecording={selectedTimeRecording}
                    editCancel={editCancel}
                  />
                </>
              )}
            {activeComponent === "timeRecordingSearch" &&
              roles.includes("admin") && (
                <TimeRecordingSearch
                  projects={projects}
                  setTimeRecording={setTimeRecording}
                  setSelectedTimeRecording={handleEditTimeRecording}
                />
              )}
            {activeComponent === "statistics" && (
              <Statistics projects={projects} />
            )}
            {activeComponent === "calculations" && roles.includes("admin") && (
              <Calculation
                projects={projects}
                employees={employees}
                recordingTypes={recordingTypes}
              />
            )}
          </div>
        </>
      ) : (
        <div>Please login to continue.</div>
      )}
    </div>
  );
}

export default App;
