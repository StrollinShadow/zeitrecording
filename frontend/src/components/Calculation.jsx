import React, { useState } from 'react';
import calculationService from '../services/calculationService';

const Calculation = ({ projects, employees, recordingTypes }) => {
  const [projectId, setProjectId] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const [recordingTypeId, setRecordingTypeId] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [totalTime, setTotalTime] = useState(null);

  const handleCalculateTime = async () => {
    try {
      let totalTime;
      if (projectId && employeeId) {
        totalTime = await calculationService.calculateTotalTimeEmployee(projectId, employeeId, startDate, endDate);
      } else if (projectId && recordingTypeId) {
        totalTime = await calculationService.calculateTotalTimeRecordingType(projectId, recordingTypeId, startDate, endDate);
      } else if (projectId) {
        totalTime = await calculationService.calculateTotalTimeProject(projectId, startDate, endDate);
      }
      setTotalTime(totalTime);
    } catch (error) {
      console.error('Error calculating total time:', error);
    }
  };

  return (
    <div>
      <h2>Berechnungen</h2>
      <div>
        <label>
          Projekt:
          <select value={projectId} onChange={(e) => setProjectId(e.target.value)}>
            <option value="">Projekt auswählen</option>
            {projects.map((project) => (
              <option key={project.id} value={project.id}>
                {project.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Mitarbeiter:
          <select value={employeeId} onChange={(e) => setEmployeeId(e.target.value)} disabled={!projectId || recordingTypeId}>
            <option value="">Mitarbeiter auswählen</option>
            {employees.map((employee) => (
              <option key={employee.id} value={employee.id}>
                {employee.firstname} {employee.lastname}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Erfassungsart:
          <select value={recordingTypeId} onChange={(e) => setRecordingTypeId(e.target.value)} disabled={!projectId || employeeId}>
            <option value="">Erfassungsart auswählen</option>
            {recordingTypes.map((type) => (
              <option key={type.id} value={type.id}>
                {type.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Startdatum:
          <input type="date" value={startDate} onChange={(e) => setStartDate(e.target.value)} />
        </label>
      </div>
      <div>
        <label>
          Enddatum:
          <input type="date" value={endDate} onChange={(e) => setEndDate(e.target.value)} />
        </label>
      </div>
      <button onClick={handleCalculateTime}>Berechnen</button>
      {totalTime !== null && (
        <div>
          <h3>Gesamte Zeit: {totalTime} Stunden</h3>
        </div>
      )}
    </div>
  );
};

export default Calculation;
