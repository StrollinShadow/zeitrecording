import React, { useState, useEffect } from "react";
import timeRecordingDataService from "../services/timeRecordingDataService";

const TimeRecordingData = ({
  employees,
  projects,
  recordingTypes,
  selectedTimeRecording,
  refreshTimeRecordings,
  
}) => {
  // Initialize state
  const [employeeId, setEmployeeId] = useState("");
  const [projectId, setProjectId] = useState("");
  const [recordingTypeId, setRecordingTypeId] = useState("");
  const [date, setDate] = useState("");
  const [duration, setDuration] = useState("");
  const [comment, setComment] = useState("");
  

  useEffect(() => {
    if (selectedTimeRecording) {
 
      // Populate form for editing
      setEmployeeId(selectedTimeRecording.employeeId || "");
      setProjectId(selectedTimeRecording.projectId || "");
      setRecordingTypeId(selectedTimeRecording.recordingTypeId || "");
      setDate(selectedTimeRecording.date || "");
      setDuration(selectedTimeRecording.duration || "");
      setComment(selectedTimeRecording.comment || "");
    } else {
      // Reset form for adding a new entry
      clearForm();
    }
  }, [selectedTimeRecording]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const newOrUpdatedTimeRecording = {
      projectId,
      employeeId,
      recordingTypeId,
      date,
      duration,
      comment,
    };

    if (selectedTimeRecording && selectedTimeRecording.timeRecordingId) {
      // Edit mode
      await timeRecordingDataService.editTimeRecording({
        ...newOrUpdatedTimeRecording,
        timeRecordingId: selectedTimeRecording.timeRecordingId,
      });
    } else {
      // Add mode
      await timeRecordingDataService.addTimeRecording(
        newOrUpdatedTimeRecording
      );
    }
    refreshTimeRecordings();
    clearForm();
  };
  // In App.jsx or the relevant parent component
 

  const clearForm = () => {
    setEmployeeId("");
    setProjectId("");
    setRecordingTypeId("");
    setDate("");
    setDuration("");
    setComment("");
  };
const handleEmployeeChange = (event) => {
  setEmployeeId(event.target.value);
}
const handleProjectChange = (event) => {
  setProjectId(event.target.value);
  
}
const handleRecordingTypeChange = (event) => {
  setRecordingTypeId(event.target.value);
}
const handleDateChange = (event) => {
  let date = event.target.value;
  setDate(date);
}
const handleDurationChange = (event) => {
  let duration = event.target.value;
  setDuration(duration);
}
const handleCommentChange = (event) => {
  let comment = event.target.value;
  setComment(comment);
}


  return (
    <form onSubmit={handleSubmit}>
      <h2>
        {selectedTimeRecording ? "Zeiterfassung bearbeiten" : "Zeiterfassung hinzufügen"}
      </h2>
      <div>
        <label>
          Projekt:
          <select
            value={projectId}
            onChange={handleProjectChange}
            required
          >
            <option value="">Projekt auswählen</option>
            {projects.map((project) => (
              <option key={project.id} value={project.id}>
                {project.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Mitarbeiter:
          <select
            value={employeeId}
            onChange={handleEmployeeChange}
            required
          >
            <option value="">Mitarbeiter auswählen</option>
            {employees.map((employee) => (
              <option key={employee.id} value={employee.id}>
                {employee.firstname} {employee.lastname}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Zeiterfassungs Typ:
          <select
            value={recordingTypeId}
            onChange={handleRecordingTypeChange}
            required
          >
            <option value="">Typ auswählen</option>
            {recordingTypes.map((type) => (
              <option key={type.id} value={type.id}>
                {type.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Datum:
          <input
            type="date"
            value={date}
            onChange={handleDateChange}
            required
          />
        </label>
      </div>
      <div>
        <label>
          Dauer (Stunden):
          <input
            type="number"
            value={parseInt(duration)}
            onChange={handleDurationChange}
            required
          />
        </label>
      </div>
      <div>
        <label>
          Kommentar:
          <input
            type="text"
            value={comment}
            onChange={handleCommentChange}
          />
        </label>
      </div>
      <button type="submit">Speichern</button>
      <button type="button" onClick={clearForm}>
        Abbrechen
      </button>
    </form>
  );
};

export default TimeRecordingData;
