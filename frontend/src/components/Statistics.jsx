import React, { useState } from 'react';
import statisticService from '../services/statisticService';

const Statistic = ({ projects }) => {
  const [projectId, setProjectId] = useState('');
  const [statisticType, setStatisticType] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [count, setCount] = useState(null);

  const handleGetCount = async () => {
    try {
      let count;
      if (statisticType === 'deleted') {
        count = await statisticService.getDeletedRecordsCount(projectId, startDate, endDate);
      } else if (statisticType === 'new') {
        count = await statisticService.getNewRecordsCount(projectId, startDate, endDate);
      } else if (statisticType === 'changed') {
        count = await statisticService.getChangedRecordsCount(projectId, startDate, endDate);
      }
      setCount(count);
    } catch (error) {
      console.error('Error getting count:', error);
    }
  };

  return (
    <div>
      <h2>Statistiken</h2>
      <div>
        <label>
          Projekt:
          <select value={projectId} onChange={(e) => setProjectId(e.target.value)}>
            <option value="">Projekt auswählen</option>
            {projects.map((project) => (
              <option key={project.id} value={project.id}>
                {project.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <label>
          Statistiktyp:
          <select value={statisticType} onChange={(e) => setStatisticType(e.target.value)}>
            <option value="">Statistiktyp auswählen</option>
            <option value="deleted">Gelöschte Zeiterfassungen</option>
            <option value="new">Neue Zeiterfassungen</option>
            <option value="changed">Geänderte Zeiterfassungen</option>
          </select>
        </label>
      </div>
      <div>
        <label>
          Startdatum:
          <input type="date" value={startDate} onChange={(e) => setStartDate(e.target.value)} />
        </label>
      </div>
      <div>
        <label>
          Enddatum:
          <input type="date" value={endDate} onChange={(e) => setEndDate(e.target.value)} />
        </label>
      </div>
      <button onClick={handleGetCount}>Anzahl abrufen</button>
      {count !== null && (
        <div>
          <h3>Anzahl: {count}</h3>
        </div>
      )}
    </div>
  );
};

export default Statistic;
