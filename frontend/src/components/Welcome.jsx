// Welcome.js
import React, { useState } from "react";

const Welcome = () => {
  return (
    <div>
      <h2>Zeiterfassung, Berechnungen und Statistiken M321</h2>
      <p>
        Es können geleistete Stunden für ein Projekt erfasst und verwaltet
        werden. Es gibt verschiedene Berechnungen und Statistiken. <br /> Die
        Authentisierung fürs Front- und Backend geschieht mit Keycloak.
      </p>
    </div>
  );
};

export default Welcome;
