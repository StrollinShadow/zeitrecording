import React, { useState } from "react";
import timeRecordingDataService from "../services/timeRecordingDataService";

const TimeRecordingSearch = ({ projects, setSelectedTimeRecording }) => {
  const [projectId, setProjectId] = useState("");
  const [timeRecordings, setTimeRecordings] = useState([]);

  const searchTimeRecordings = (event) => {
    event.preventDefault();
    if (projectId) {
      timeRecordingDataService
        .searchTimeRecordings(projectId)
        .then(setTimeRecordings)
        .catch((error) => console.error("Search failed", error));
    }
  };

  const handleProjectChange = (event) => {
    setProjectId(event.target.value);
  };
  const handleEdit = (recording) => {
    setSelectedTimeRecording(recording);
  };
  return (
    <div>
      <form onSubmit={searchTimeRecordings}>
        <label>
          Projekt:
          <select onChange={handleProjectChange} value={projectId}>
            <option value="">Projekt auswählen</option>
            {projects.map((project) => (
              <option key={project.id} value={project.id}>
                {project.name}
              </option>
            ))}
          </select>
        </label>
        <button type="submit">Suchen</button>
      </form>

      {timeRecordings.length > 0 ? (
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Projekt</th>
              <th>Mitarbeiter</th>
              <th>Typ</th>
              <th>Datum</th>
              <th>Dauer</th>
              <th>Kommentar</th>
              <th>Aktionen</th>
            </tr>
          </thead>
          <tbody>
            {timeRecordings.map((recording) => (
              <tr key={recording.timeRecordingId}>
                <td>{recording.timeRecordingId}</td>
                <td>{recording.project}</td>
                <td>{recording.employee}</td>
                <td>{recording.recordingType}</td>
                <td>{recording.date}</td>
                <td>{recording.duration}</td>
                <td>{recording.comment}</td>
                <td>
                  <button onClick={() => handleEdit(recording)}>
                    Bearbeiten
                  </button>
                  <button
                    onClick={() =>
                      timeRecordingDataService
                        .deleteTimeRecording(recording)
                        .then(() =>
                          searchTimeRecordings({ preventDefault: () => {} })
                        ) // Refresh list
                        .catch((error) => console.error("Delete failed", error))
                    }
                  >
                    Löschen
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p>Keine Zeiterfassung für gewähltes Projekt gefunden</p>
      )}
    </div>
  );
};

export default TimeRecordingSearch;
