// Menu.js
import React, { useState } from "react";

const Menu = ({ setActiveComponent, roles }) => {
  return (
    <nav>
      <button onClick={() => setActiveComponent("welcome")}>Willkommen</button>
      {roles.includes("admin") && (
        <>
          <button onClick={() => setActiveComponent("timeRecordingInput")}>
            Zeiterfassung Dateneingabe
          </button>
          <button onClick={() => setActiveComponent("timeRecordingSearch")}>
            Zeiterfassung Suche
          </button>
        </>
      )}
      <button onClick={() => setActiveComponent("statistics")}>
        Statistiken
      </button>
      {roles.includes("admin") && (
        <button onClick={() => setActiveComponent("calculations")}>
          Berechnungen
        </button>
      )}
    </nav>
  );
};

export default Menu;
