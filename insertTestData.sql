
INSERT INTO robin_employee (firstname, lastname) VALUES
('Steve', 'Merson'),
('Bobin', 'Bobt'),
('John', 'Pork');


INSERT INTO robin_project (name) VALUES
('Project 1'),
('Project 2'),
('Project 3');


INSERT INTO robin_recording_type (name) VALUES
('Entwicklung'),
('Dokumentation'),
('Meeting'),
('Urlaub');


INSERT INTO robin_statistics_type (name) VALUES
('Gelöscht'),
('Neu'),
('Geändert');
