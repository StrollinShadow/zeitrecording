USE sql11690338 ;

DROP TABLE IF EXISTS robin_employee ;

CREATE TABLE robin_employee (
  id INT NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(100) NULL,
  lastname VARCHAR(100) NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


DROP TABLE IF EXISTS robin_project ;

CREATE TABLE robin_project (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


DROP TABLE IF EXISTS robin_recording_type ;

CREATE TABLE robin_recording_type (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


DROP TABLE IF EXISTS robin_statistics ;

CREATE TABLE robin_statistics (
  statisticId INT UNSIGNED NOT NULL AUTO_INCREMENT,
  projectId INT NOT NULL,
  statisticTypeId INT NOT NULL,
  statisticDate DATE NOT NULL,
  PRIMARY KEY (statisticId))
ENGINE = InnoDB;


DROP TABLE IF EXISTS robin_statistics_type ;

CREATE TABLE robin_statistics_type (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


DROP TABLE IF EXISTS robin_timerecording ;

CREATE TABLE robin_timerecording (
  timeRecordingId INT UNSIGNED NOT NULL AUTO_INCREMENT,
  projectId INT NOT NULL,
  employeeId INT NOT NULL,
  recordingTypeId INT NOT NULL,
  date DATE NOT NULL,
  duration DECIMAL(10,2) NOT NULL,
  comment VARCHAR(255) NULL,
  PRIMARY KEY (timeRecordingId))
ENGINE = InnoDB;

